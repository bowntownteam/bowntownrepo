-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 01, 2017 at 10:50 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_brown_town`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category`) VALUES
(1, 'Today\'s Special'),
(2, 'Brown Town Offer'),
(3, 'Hot Beverage'),
(4, 'Cold Beverage'),
(6, 'Pizza'),
(7, 'Main Course'),
(5, 'Sandwich & Burger'),
(8, 'Dessert');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback`
--

CREATE TABLE `tbl_feedback` (
  `id` int(11) NOT NULL,
  `feedback` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `usertype` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `username`, `password`, `usertype`) VALUES
(1, 'admin', '$2y$10$cfDQe72UO7QeFpbbQt3XX.cfIl36szCR1TJ7E2eNff2C6kEP.KgfC', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `product_name` text NOT NULL,
  `rate` float NOT NULL,
  `calorie` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `today` date NOT NULL,
  `image_url` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `type_id`, `product_name`, `rate`, `calorie`, `description`, `today`, `image_url`) VALUES
(1, 15, 'Tex-Mex Burger', 260, '', 'Two American regions flavour infused  in fillet of chicken cooked in a griller along with sauteed mushroom and ranch dressing', '2017-06-01', 'images/f2df8ac29ce4c473155fa07666a54e86.jpg'),
(52, 16, 'Mobile', 60, '', '', '2017-06-01', 'images/8bd7044c9ef556f62f59bee765f48f3b.JPG'),
(36, 1, 'Virgin mojito', 90, '', '', '2017-05-23', 'images/60caee26c391fe6670b3ea3ede895113.JPG'),
(51, 12, 'Full house 1', 0, '', '', '2017-06-01', 'images/c5eb6f182ace8e686975a2d91156ef43.JPG'),
(50, 12, 'Full house ', 380, '', '', '2017-05-29', 'images/8d5d9337d12a992d8525b21741ce74b3.JPG'),
(20, 20, 'BlueBerry crumble', 0, '', '', '2017-05-23', 'images/0a6e760f8c4c16e4789dea331fd20be3.JPG'),
(21, 18, 'Waffles with ice cream', 140, '', '', '2017-05-23', 'images/219261ac786d84a442b16c5271e726a8.JPG'),
(16, 6, 'Fire House Burger', 260, '', '', '2017-05-23', 'images/d57557ca284bbf7e1836e83dd799e66b.JPG'),
(17, 6, 'Classic Country Style Burger', 260, '', '', '2017-05-23', 'images/ad719965b96cd730f08d6753f192b011.JPG'),
(27, 8, 'Salad', 0, '', '', '2017-05-23', 'images/8839a9b3bc017a3b9e1bb5ced2c8f1d1.JPG'),
(28, 18, 'White Chocolate Bread Butter Pudding', 80, '', '', '2017-05-23', 'images/e85dcdb3c9dfa4da1372b8e4c8fe496f.JPG'),
(37, 1, 'Strawberry shake', 140, '', '', '2017-05-23', 'images/0b10127630f77af5843dc5ad75311a26.JPG'),
(60, 11, 'Apple', 50, '', '', '2017-06-01', 'images/f52323320a80786ecbb69e397aaea78a.JPG'),
(42, 20, 'Red Velvet', 100, '', '', '2017-05-23', 'images/a4171602bd45b2f37691cd882570ef30.JPG'),
(29, 9, 'Penne Pasta Chicken', 220, '', '', '2017-05-23', 'images/477b9cf6db4df10d33efd46a24e22c77.JPG'),
(14, 12, 'Pizza', 290, '', '', '2017-05-20', 'images/6c82be2446457a5104808d1cf59c8265.jpg'),
(46, 17, 'Coffee', 35, '', '', '2017-06-01', 'images/44bb90512bc10f8cc789b1fc9a22d195.jpg'),
(15, 5, 'Florentine burger', 180, '', '', '2017-05-23', 'images/43465849dcaaf6be6d9da0cf1797b89b.JPG'),
(18, 6, 'Steak Burger', 280, 'Beef Burger', '', '2017-05-23', 'images/5ff4c6d36f43ebd290d07cf7872e7ac2.JPG'),
(19, 6, 'Tex-Mex Burger', 260, 'Chicken', '', '2017-05-23', 'images/651f1bcfb7f853e1c5adaf96ee776a2a.JPG'),
(22, 20, 'Tiramisu ', 110, '', '', '2017-05-23', 'images/25320d841553f03ebe86bb1e101fad7f.JPG'),
(23, 18, 'Sizzler Brownie', 150, '', '', '2017-05-23', 'images/d2d53e2a2b973f493208b964f0dba8d8.JPG'),
(24, 6, 'Chk Croissant Sandwich', 180, '', '', '2017-05-23', 'images/1117efae33b34ed545e4bfcd119bb02e.JPG'),
(25, 6, 'Brown Town\'s Crown Sandwich', 300, '', '', '2017-05-23', 'images/77664221b15da10ebc43dd19b228501e.JPG'),
(26, 6, 'Chipolata Chicken Sausage Sub', 220, '', '', '2017-05-23', 'images/91951a3547a471843624b4aaaea4d029.JPG'),
(30, 20, 'Macroons', 40, '', '', '2017-05-23', 'images/5da1a4b6ba800e7010a849fdf40feca2.JPG'),
(31, 18, 'Chocolate Lava', 70, '', '', '2017-05-23', 'images/3c67641f16512e6654cfdf286f64cc61.JPG'),
(32, 18, 'Falooda', 140, '', '', '2017-05-23', 'images/ec1698589eec9e2ce1eec3c837f3e631.JPG'),
(33, 2, 'ABC', 130, 'Apple Beatroot Carrot', '', '2017-05-23', 'images/3d6d5fd8ad2c9c026e4518097e4a1b88.JPG'),
(34, 1, 'Strawberry Mojito', 120, '', '', '2017-05-23', 'images/ed34ad4e6979c8bb406f94fb6454b321.JPG'),
(35, 1, 'Passion fruit juice', 140, '', '', '2017-05-23', 'images/8393465c708f592b3b8db114235fa0e8.JPG'),
(38, 20, 'Cinnamon Roll', 60, '', '', '2017-05-23', 'images/4ac423319dafb1d778d5396ede74ee4e.JPG'),
(39, 15, 'Butter', 250, 'This is good food and better calorie', '', '2017-06-01', 'images/8fec562957b5ead1f7d8a225fe04f9f0.JPG'),
(40, 5, 'Cheese chilli toast', 90, '', '', '2017-05-23', 'images/61f0f2807cbf46553e01802eabc3dc36.JPG'),
(41, 20, 'White vancho', 70, '', '', '2017-05-23', 'images/b4966d18037c49f9b138cfb5186bf10a.JPG'),
(43, 20, 'Dark Vancho', 70, '', '', '2017-05-23', 'images/d4e13d3323648f426f2f37d12b3df8ee.JPG'),
(44, 18, 'Chocolate Brownee ', 70, '', '', '2017-05-23', 'images/70ff6b43b2ccf0e56ebb91e5762fa08e.JPG'),
(59, 17, 'Tea', 30, '', '', '2017-05-31', 'images/63f9eb0c47530b08920e92ce773262a9.JPG'),
(61, 17, 'Tea2', 35, '', '', '2017-06-01', 'images/5dea29657f463f8f9e83df292a715274.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE `tbl_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`id`, `type`, `category_id`) VALUES
(1, 'MOCKTAIL', 4),
(2, 'FRESH JUICE', 4),
(3, 'CHEF\'S INNOVATION', 4),
(5, 'VEGETARIAN', 5),
(6, 'NON-VEGETARIAN', 5),
(7, 'STARTER', 7),
(8, 'SOUP AND SALAD', 7),
(9, 'PASTA', 7),
(10, 'STEAK', 7),
(11, 'VEGETARIAN', 6),
(12, 'NON-VEGETARIAN', 6),
(15, 'TODAY\'S SPECIAL', 1),
(16, 'BROWN TOWN OFFER', 2),
(17, 'HOT BEVERAGE', 3),
(18, 'DESSERT', 8),
(20, 'PASTRY', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `tbl_type`
--
ALTER TABLE `tbl_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
