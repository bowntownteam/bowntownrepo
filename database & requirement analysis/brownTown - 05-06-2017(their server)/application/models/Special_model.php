<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 23-04-2017
 * Time: 12:38 AM
 */
class Special_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_types(){
        $this->db->SELECT(TABLE_TYPE.'.*');
        $this->db->FROM(TABLE_TYPE);
        $this->db->WHERE(TABLE_TYPE.'.category_id='.SPECIAL);
        $query = $this->db->get();
        return $query->result_array();

    }

    public function numOfRows()
    {
        $types = array();
        $type_result = $this->get_types();
        foreach ($type_result as $type){
            array_push($types,$type['id']);
        }
        $type_string = implode(',',$types);

        $this->db->SELECT(TABLE_PRODUCT.'.id');
        $this->db->FROM(TABLE_PRODUCT);
        $this->db->WHERE(TABLE_PRODUCT.'.type_id IN ('.$type_string.')');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_product()
    {
        $types = array();
        $type_result = $this->get_types();
        foreach ($type_result as $type){
            array_push($types,$type['id']);
        }
        $type_string = implode(',',$types);

        $this->db->SELECT(TABLE_PRODUCT.'.*,'.TABLE_TYPE.'.type');
        $this->db->FROM(TABLE_PRODUCT);
        $this->db->JOIN(TABLE_TYPE,TABLE_TYPE.'.id='.TABLE_PRODUCT.'.type_id');
        $this->db->WHERE(TABLE_PRODUCT.'.type_id IN ('.$type_string.')');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insertData($data)
    {
        $result = $this->db->insert(TABLE_PRODUCT,$data);
        return $result;
    }

    public function getOneType($data){
        $query = $this->db->get_where(TABLE_PRODUCT,$data);
        return $query->result();
    }

    Public function updateData($data,$editId)
    {
        $this->db->WHERE('id',$editId);
        $query = $this->db->UPDATE(TABLE_PRODUCT,$data);
        return $query;
    }
    Public function deleteData($deleteId)
    {
        $this->db->WHERE('id',$deleteId);
        $query = $this->db->DELETE(TABLE_PRODUCT);
        return $query;
    }

    //for unlinking the image while today's special delete time.
    public function unlinkProImage($deleteId)
    {
        $this->db->SELECT('image_url');
        $this->db->WHERE('id',$deleteId);
        $this->db->FROM(TABLE_PRODUCT);
        $query = $this->db->get();
        return $query->result();
    }
}