<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 12:48 PM
 */
class Category_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function all_category(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_CATEGORY);
        $query = $this->db->get();
        return $query->result_array();
    }


}