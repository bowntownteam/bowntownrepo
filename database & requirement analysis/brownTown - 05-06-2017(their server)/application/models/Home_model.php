<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 12:11 PM
 */
class Home_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function category_count(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_CATEGORY);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function product_count(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_PRODUCT);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function total_feedback_count(){
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_FEEDBACK);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function feedback($feedback){
        $percentage = 0;
        $this->db->SELECT('*');
        $this->db->FROM(TABLE_FEEDBACK);
        $this->db->LIKE('feedback',$feedback);
        $query = $this->db->get();
        $feedback_count =  $query->num_rows();
        $total_feedback_count = $this->total_feedback_count();
        if($total_feedback_count!=0){
            $percentage = ($feedback_count/$total_feedback_count)*100;
        }
        return ($percentage);
    }

    public function feedback_delete_all(){
        if($this->db->empty_table(TABLE_FEEDBACK)){
            return true;
        }
        else{
            return false;
        }
    }
}