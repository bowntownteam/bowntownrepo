<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 01:24 PM
 */
class Type extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Type_model');
        $this->load->model('Category_model');
        $this->load->library('pagination');

        //for session checking
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }

    public function index(){
        $num_rows = $this->Type_model->numofRows();

        $config['base_url']     =  base_url().'index.php/type/index';
        $config['total_rows']   = $num_rows;
        $config['per_page']     = '10';

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $record['type'] = $this->Type_model->get_all_type($config['per_page'],$this->uri->segment(3));

        $this->load->view('admin/header');
        $this->load->view('admin/type/index',$record);
        $this->load->view('admin/footer');
    }

    public function insertdata()
    {
        $this->form_validation->set_rules('type','Type','required');
        $data['category'] = $this->Category_model->all_category();
        if($this->form_validation->run()===false)
        {
            $this->load->view('admin/header');
            $this->load->view('admin/type/add.php',$data);
            $this->load->view('admin/footer');
        }
        else
        {
            $data = array(
                'type'=>$this->input->post('type'),
                'category_id'=>$this->input->post('category')
            );

            $success = $this->Type_model->insertData($data);
            if($success)
            {
                $this->session->set_flashdata("flash",["type"=>"success", "message"=>"Data inserted successfully"]);
                redirect('type');
            }
            else
            {
                $this->session->set_flashdata("flash",["type" => "danger","message" => "Something went wrong"]);
                redirect('type');
            }
        }
    }

    public function update_view()
    {
        $editId = $this->uri->segment(3);
        $data = array('id'=>$editId);
        $result['records'] = $this->Type_model->getOneType($data);
        $result['category'] = $this->Category_model->all_category();
        $this->load->view('admin/header');
        $this->load->view('admin/type/edit',$result);
        $this->load->view('admin/footer');
    }

    public function update()
    {
        $this->form_validation->set_rules('type','Type','required');
        $data['category'] = $this->Category_model->all_category();
        if($this->form_validation->run()===false)
        {
            $this->load->view('admin/header');
            $this->load->view('admin/type/add.php',$data);
            $this->load->view('admin/footer');
        }
        else
        {
            $editId = $this->input->post('editId');
            $data = array(
                'type'=>$this->input->post('type'),
                'category_id'=>$this->input->post('category')
            );

            $success = $this->Type_model->updateData($data,$editId);
            if($success)
            {
                $this->session->set_flashdata("flash",["type"=>"success", "message"=>"Data updated successfully"]);
                redirect('type');
            }
            else
            {
                $this->session->set_flashdata("flash",["type" => "danger","message" => "Something went wrong"]);
                redirect('type');
            }
        }
    }
    function deletedata()
    {
        $deleteId = $this->uri->segment(3);

        $success = $this->Type_model->deleteData($deleteId);
        if($success) {
            $this->session->set_flashdata("flash",["type" => "success","message" => "Data Deleted Successfully"]);
            redirect('type');
        }
        else {
            $this->session->set_flashdata("flash",["type" => "danger","message" => "Something went wrong"]);
            redirect('type');
        }
    }

}