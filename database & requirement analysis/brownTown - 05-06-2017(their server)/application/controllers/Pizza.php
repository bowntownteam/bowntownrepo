<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 23-04-2017
 * Time: 12:34 AM
 */
class Pizza extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Type_model');
        $this->load->model('Category_model');
        $this->load->model('Pizza_model');
        $this->load->library('pagination');
        $this->load->library('upload');

        //for session checking
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }

    public function index(){
        $num_rows_array = $this->Pizza_model->numofRows();
        $num_rows = count($num_rows_array);

        $config['base_url']     =  base_url().'index.php/pizza/index';
        $config['total_rows']   = $num_rows;
        $config['per_page']     = '10';

        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $record['product'] = $this->Pizza_model->get_all_product($config['per_page'],$this->uri->segment(3));

        $this->load->view('admin/header');
        $this->load->view('admin/pizza/index',$record);
        $this->load->view('admin/footer');
    }

    public function insertdata()
    {
        $this->form_validation->set_rules('name','Product name','required');
        $this->form_validation->set_rules('type','Type','required');

        $data['types'] = $this->Type_model->types_by_condition(PIZZA);

        if($this->form_validation->run()===false)
        {
            $this->load->view('admin/header');
            $this->load->view('admin/pizza/add',$data);
            $this->load->view('admin/footer');
        }
        else
        {
            $fileUpload=array();
            $isUpload=FALSE;
            $up_image=array(
                'upload_path'=>'./images/',
                'allowed_types'=>'jpg|jpeg|png|gif',
                'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('file')){
                $fileUpload=$this->upload->data();
                $isUpload=TRUE;
                $image  	=	"images/".$fileUpload['file_name'];
            }
            $data1 = array(
                'type_id'=>$this->input->post('type'),
                'product_name'=>$this->input->post('name'),
                'rate'=>$this->input->post('rate'),
                'calorie'=>$this->input->post('calorie'),
                'description'=>$this->input->post('description'),
                'today'=>date('Y-m-d', strtotime($this->input->post('today'))),
                'image_url'=>$image
            );

            $success = $this->Pizza_model->insertData($data1);
            if($success)
            {
                $this->session->set_flashdata("flash",["type"=>"success", "message"=>"Data inserted successfully"]);
                redirect('pizza_ad');
            }
            else
            {
                $this->session->set_flashdata("flash",["type" => "danger","message" => "Something went wrong"]);
                redirect('pizza_ad');
            }
        }
    }

    public function update_view()
    {
        $editId = $this->uri->segment(3);
        $data = array('id'=>$editId);
        $result['records']  = $this->Pizza_model->getOneType($data);
        $result['types']    = $this->Type_model->types_by_condition(PIZZA);
        $this->load->view('admin/header');
        $this->load->view('admin/pizza/edit',$result);
        $this->load->view('admin/footer');
    }

    public function update()
    {
        $this->form_validation->set_rules('name','Product name','required');
        $this->form_validation->set_rules('type','Type','required');

        $data['types'] = $this->Type_model->types_by_condition(PIZZA);
        if($this->form_validation->run()===false)
        {
            $this->load->view('admin/header');
            $this->load->view('admin/pizza/edit.php',$data);
            $this->load->view('admin/footer');
        }
        else
        {
            $editId = $this->input->post('typeId');
            $fileUpload=array();
            $isUpload=FALSE;
            $up_image=array(
                'upload_path'=>'./images/',
                'allowed_types'=>'jpg|jpeg|png|gif',
                'encrypt_name'=>TRUE
            );
            $this->upload->initialize($up_image);
            if($this->upload->do_upload('file')){
                $fileUpload=$this->upload->data();
                $isUpload=TRUE;
                $image  	=	"images/".$fileUpload['file_name'];
                $data1['image_url'] =$image;
            }
            $data1['type_id'] =$this->input->post('type');
            $data1['product_name'] =$this->input->post('name');
            $data1['rate'] =$this->input->post('rate');
            $data1['calorie'] =$this->input->post('calorie');
            $data1['description'] =$this->input->post('description');
            $data1['today'] =date('Y-m-d', strtotime($this->input->post('today')));

            $success = $this->Pizza_model->updateData($data1,$editId);
            if($success)
            {
                $this->session->set_flashdata("flash",["type"=>"success", "message"=>"Data updated successfully"]);
                redirect('pizza_ad');
            }
            else
            {
                $this->session->set_flashdata("flash",["type" => "danger","message" => "Something went wrong"]);
                redirect('pizza_ad');
            }
        }
    }

    function deletedata()
    {
        $deleteId = $this->uri->segment(3);
        $image = $this->Pizza_model->unlinkProImage($deleteId);
        if(isset($image)) {
            $newsImg = $image[0]->image_url;
            if (file_exists($newsImg)) {
                unlink($newsImg);
            }
        }
        $success = $this->Pizza_model->deleteData($deleteId);
        if($success) {
            $this->session->set_flashdata("flash",["type" => "success","message" => "Data Deleted Successfully"]);
            redirect('pizza_ad');
        }
        else {
            $this->session->set_flashdata("flash",["type" => "danger","message" => "Something went wrong"]);
            redirect('pizza_ad');
        }
    }
}