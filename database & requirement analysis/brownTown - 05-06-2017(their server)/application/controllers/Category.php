<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 12:41 PM
 */
class Category extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Category_model');
        //for session checking
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }

    public function index(){
       $record['category'] = $this->Category_model->all_category();

       $this->load->view('admin/header');
       $this->load->view('admin/category/index',$record);
       $this->load->view('admin/footer');
    }
}