    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Powered By</b> <a href="http://bodhiinfo.com" target="_blank"><img src="<?= base_url(); ?>img/bodhi_logo.png" alt="Bodhi"></a>
        </div>
        <strong>Copyright &copy; <script>document.write(new Date().getFullYear());</script> <a href="<?= base_url(); ?>">Brown Town</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.4 -->
<script src="<?= base_url(); ?>js/jquery-2.2.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>js/bootstrap.min.js"></script>
<!-- datepicker -->
<script src="<?= base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>js/app.min.js"></script>
<script src="<?= base_url(); ?>js/custom.js"></script>
<script src="http://www.jqueryscript.net/demo/Circular-Pie-Chart-Progress-Bar-Plugin-with-jQuery-Canvas/js/pie-chart.js" type="text/javascript"></script>
<script>
    $('#example').pieChart({
                barColor: '#8bc34a',
                trackColor: '#e6e6e6',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });
    $('#example_1').pieChart({
                barColor: '#8465d4',
                trackColor: '#e6e6e6',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });
    $('#example_2').pieChart({
                barColor: '#f44336',
                trackColor: '#e6e6e6',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });
</script>
</body>
</html>
