<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Today's Special
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Update Product</h3>
                    </div>
                    <div class="box-body">
                        <?php foreach ($records as $key){?>
                        <form action="<?php echo site_url(); ?>/special/edit/" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="hidden" name="typeId" id="typeId" value="<?= $key->id;?>">
                                    <div class="form-group">
                                        <label for="category">Type<span style="color: #CC0000" >*</span></label>
                                        <select class="form-control" name="type" required>
                                            <option value="">select</option>
                                            <?php foreach ($types as $type){?>
                                                <option value="<?php echo $type['id']?>" <?php if($key->type_id==$type['id']){echo 'selected';}?>><?php echo $type['type']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Product name<span style="color: #CC0000" >*</span></label>
                                        <input type="text" name="name" id="name" class="form-control"  value="<?= $key->product_name;?>" required />
                                    </div>
                                    <div class="form-group">
                                        <label for="rate">Rate</label>
                                        <input type="text" name="rate" id="rate" class="form-control" value="<?= $key->rate;?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="calorie">Short description<span style="color: #CC0000" ></span></label>
                                        <input type="text" name="calorie" id="calorie" class="form-control" value="<?= $key->calorie;?>"  />
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description" id="description" class="form-control"  ><?= $key->description;?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="today">Date<span style="color: #CC0000" ></span></label>
                                        <input type="text" name="today" id="today" class="form-control datepicker" value="<?=date('d-m-Y', strtotime($key->today));?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="file">Upload Image<span style="color: #CC0000" >*</span></label>
                                        <input type="file" name="file" id="file"  />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
