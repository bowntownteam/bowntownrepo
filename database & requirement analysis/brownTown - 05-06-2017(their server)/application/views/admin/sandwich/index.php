<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Sandwich & Burger
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Products</h3>
                        <!-- for seession message -->
                        <?php if($this->session->flashdata('flash')) { ?>
                            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                                <?= $this->session->flashdata('flash')['message']; ?>
                            </div>
                        <?php } ?>
                        <span class="pull-right"><a href="<?php echo site_url();?>/sandwich/add" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Sub category</th>
                                <th>Product</th>
                                <td>Rate</td>
                                <td>Short description</td>
                                <td>Description</td>
                                <td>Image</td>
                                <td>Options</td>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php 
                            
							$i = 1;
							if(count($product)>0){
                                foreach($product as $r) {
                                    $tab_id = $r['id'];
                                ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $r['type']; ?></td>
                                        <td><?= $r['product_name']; ?></td>
                                        <td><?= $r['rate']; ?></td>
                                        <td><?= $r['calorie']; ?></td>
                                        <td><?= $r['description']; ?></td>
                                        <td><img src="<?= base_url(); ?><?= $r['image_url']; ?>" /></td>
                                        <td><a href="<?= site_url(); ?>/sandwich/delete/<?= $tab_id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()">Delete</a>
                                            <a href="<?= site_url(); ?>/sandwich/editview/<?= $tab_id; ?>" class="btn btn-success btn-flat">Edit</a>
                                        </td>

                                    </tr>
                                <?php }
							} else {
							    ?>
                                <tr><td colspan="8" align="center">Product list is empty</td></tr>
                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>