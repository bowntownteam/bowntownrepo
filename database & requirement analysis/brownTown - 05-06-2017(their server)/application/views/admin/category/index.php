<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
           Product Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Product Category</h3>
                      
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Category</th>
<!--                                <td>Types</td>-->
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php 
                            
							$i = 1;
							if(count($category)>0){
                                foreach($category as $r) {
                                ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $r['category']; ?></td>
<!--                                        <td>-->
<!--                                            <a href="--><?//= site_url(); ?><!--/type/--><?//= $r['id']?><!--" class="btn btn-info btn-link"> Sub Category </a>-->
<!--                                        </td>-->

                                    </tr>
                                <?php }
							} else {
							    ?>
                                <tr><td colspan="2" align="center">Category list is empty</td></tr>
                                <?php
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
</div>
