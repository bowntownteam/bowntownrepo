<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 18-04-2017
 * Time: 12:15 PM
 */

require (APPPATH.'/libraries/REST_Controller.php');
class Brown_Products extends REST_Controller{
//class Brown_Products extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Get_All_Products');
        $this->load->model('Feedback_model');
    }

    //today's special
    public function special_get(){
        $today = date("Y-m-d");
        $types = $this->Get_All_Products->get_types(SPECIAL);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId,$today);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];

                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's offer
    public function offer_get(){
        $today = date("Y-m-d");
        $types = $this->Get_All_Products->get_types(OFFER);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId,$today);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's beverages
    public function beverages_get(){
        $types = $this->Get_All_Products->get_types(BEVERAGES);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's mocktail
    public function mocktail_get(){
        $types = $this->Get_All_Products->get_types(MOCKTAIL);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's pizza
    public function pizza_get(){
        $types = $this->Get_All_Products->get_types(PIZZA);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's burger
    public function burger_get(){
        $types = $this->Get_All_Products->get_types(BURGER);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's sandwich
    public function sandwich_get(){
        $types = $this->Get_All_Products->get_types(SANDWICH);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //today's dessert
    public function dessert_get(){
        $types = $this->Get_All_Products->get_types(DESSERT);
        $record = array();
        foreach ($types as $type){
            $typeId = $type['id'];
            $typeArray['typeName']= $type['type'];
            $typeArray['product'] = array();
            $products = $this->Get_All_Products->get_products($typeId);
            foreach ($products as $product){
                $data['productName']    = $product['product_name'];
                $data['rate']           = $product['rate'];
                $data['calorie']        = $product['calorie'];
                $data['imageUrl']       = IMAGE_BASE_URL.$product['image_url'];
                $data['description']    = $product['description'];
                array_push($typeArray['product'],$data);
            }
            if(!empty($typeArray['product'])){
                array_push($record,$typeArray);
            }
        }
        $this->response($record);
    }

    //feedback
    public function feedback_post(){
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $feedback = array('feedback'=>$params['feedback']);
        $success = $this->Feedback_model->insert_feedback($feedback);
        if($success){
            $record=array('success');
        }
        else{
            $record=array('failed');
        }
        $this->response($record);
    }
}