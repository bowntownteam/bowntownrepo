<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 18-04-2017
 * Time: 01:30 PM
 */
class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Home_model');
        //for session checking
        if(empty($this->session->userdata("user_id")))
        {
            $this->session->set_flashdata("flash",["type"=>"danger","message"=>"Session out.!"]);
            redirect(site_url(),'refresh');
        }
    }
    public function index(){
        $record['category'] = $this->Home_model->category_count();
        $record['product'] = $this->Home_model->product_count();
        $record['feedback']['love'] = $this->Home_model->feedback('love');
        $record['feedback']['happy'] = $this->Home_model->feedback('happy');
        $record['feedback']['angry'] = $this->Home_model->feedback('angry');
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard/index',$record);
        $this->load->view('admin/footer');
    }

    public function clear_feedback(){
        $success = $this->Home_model->feedback_delete_all();
        if($success) {
            $this->session->set_flashdata("flash",["type" => "success","message" => "Removed all feedback"]);
            redirect('home');
        }
        else {
            $this->session->set_flashdata("flash",["type" => "danger","message" => "Sorry! Something went wrong"]);
            redirect('home');
        }
    }

}