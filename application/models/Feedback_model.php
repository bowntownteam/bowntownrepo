<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 04:51 PM
 */
class Feedback_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insert_feedback($feedback){
        $query = $this->db->insert(TABLE_FEEDBACK,$feedback);
        return $query;
    }
}