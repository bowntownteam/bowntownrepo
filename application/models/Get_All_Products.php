<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 18-04-2017
 * Time: 12:39 PM
 */

class Get_All_Products extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function get_types($catagory){
        //get type
        $this->db->SELECT(TABLE_TYPE.'.*');
        $this->db->FROM(TABLE_TYPE);
        $this->db->WHERE('category_id',$catagory);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_products($type,$today=null){
        if($today!=null){
            $array = array('type_id' => $type, 'today' => $today);
        }
        else{
            $array = array('type_id' => $type);
        }
        $this->db->SELECT(TABLE_PRODUCT.'.*');
        $this->db->FROM(TABLE_PRODUCT);
        $this->db->WHERE($array);
        $query = $this->db->get();
        return $query->result_array();
    }
}