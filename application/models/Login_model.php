<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 20-04-2017
 * Time: 05:19 PM
 */
class Login_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function resolve_user_login($userName, $password){
        $this->db->SELECT('password');
        $this->db->FROM(TABLE_LOGIN);
        $this->db->WHERE('username',$userName);

        $hash = $this->db->get()->row('password');

        $confirm = $this->verify_password_hash($password,$hash);
        if($confirm){
            $this->db->SELECT('id');
            $this->db->SELECT('usertype');
            $this->db->FROM(TABLE_LOGIN);
            $this->db->WHERE('username',$userName);
            return $this->db->get()->row();
        }else{
            return 0;
        }
    }
    private function verify_password_hash($password, $hash) {
        return password_verify($password, $hash);
    }

    public function get_user($user_id) {
        $this->db->from(TABLE_LOGIN);
        $this->db->where('id', $user_id);
        return $this->db->get()->row();

    }
    public function checkPasswordMatch ($password, $hash) {
        return $this->verify_password_hash($password, $hash);

    }

    public function updatePassword ($user_id, $password) {
        $data = ["password" => $this->hash_password($password)];
        $this->db->set($data);
        $this->db->where('id', $user_id);
        if ($this->db->update(TABLE_LOGIN)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    private function hash_password($password) {

        return password_hash($password, PASSWORD_BCRYPT);
    }
}