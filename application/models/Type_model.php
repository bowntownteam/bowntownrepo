<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: bodhi
 * Date: 21-04-2017
 * Time: 02:27 PM
 */
class Type_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_all_type($limit,$offset){
        $this->db->SELECT(TABLE_TYPE.'.*,'.TABLE_CATEGORY.'.category');
        $this->db->FROM(TABLE_TYPE);
        $this->db->JOIN(TABLE_CATEGORY,TABLE_CATEGORY.'.id='.TABLE_TYPE.'.category_id');
        $this->db->LIMIT($limit,$offset);
        $this->db->ORDER_BY("category_id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function types_by_condition($category){
        $this->db->SELECT(TABLE_TYPE.'.*,'.TABLE_CATEGORY.'.category');
        $this->db->FROM(TABLE_TYPE);
        $this->db->JOIN(TABLE_CATEGORY,TABLE_CATEGORY.'.id='.TABLE_TYPE.'.category_id');
        $this->db->WHERE(TABLE_TYPE.'.category_id='.$category);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insertData($data)
    {
        $result = $this->db->insert(TABLE_TYPE,$data);

        return $result;
    }
    //Return total number of rows in a table
    public function numOfRows()
    {
        return $this->db->count_all(TABLE_TYPE);
    }

    public function getOneType($data){
        $query = $this->db->get_where(TABLE_TYPE,$data);
        return $query->result();
    }

    Public function updateData($data,$editId)
    {
        $this->db->WHERE('id',$editId);
        $query = $this->db->UPDATE(TABLE_TYPE,$data);
        return $query;
    }
    Public function deleteData($deleteId)
    {
        $this->db->WHERE('id',$deleteId);
        $query = $this->db->DELETE(TABLE_TYPE);
        return $query;
    }
}