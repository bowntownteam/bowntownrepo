<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// for service
$route['todays_special'] = 'Brown_Products/special';
$route['todays_offer'] = 'Brown_Products/offer';
$route['beverages'] = 'Brown_Products/beverages';
$route['mocktail'] = 'Brown_Products/mocktail';
$route['pizza'] = 'Brown_Products/pizza';
$route['burger'] = 'Brown_Products/burger';
$route['sandwich'] = 'Brown_Products/sandwich';
$route['dessert'] = 'Brown_Products/dessert';
$route['feedback'] = 'Brown_Products/feedback';

// admin panel
$route['admin']                 = 'Login/index';
$route['home']                  = 'Home/index';
$route['home/feedclear']        = 'Home/clear_feedback';
$route['category']              = 'Category/index';
$route['type/add']              = 'Type/insertdata';
$route['type/edit_view/(:any)'] = 'Type/update_view/$i';
$route['type/editdata']         = 'Type/update';
$route['type/delete/(:any)']    = 'Type/deletedata/$i';
$route['type/(:any)']           = 'Type/index/$i';

$route['special/delete/(:any)']     = 'Todays_Special/deletedata/$i';
$route['special/edit']              = 'Todays_Special/update';
$route['special/editview/(:any)']   = 'Todays_Special/update_view/$i';
$route['special/add']               = 'Todays_Special/insertdata';
$route['special']                   = 'Todays_Special/index';

$route['offer/delete/(:any)']     = 'Offer/deletedata/$i';
$route['offer/edit']              = 'Offer/update';
$route['offer/editview/(:any)']   = 'Offer/update_view/$i';
$route['offer/add']               = 'Offer/insertdata';
$route['offer']                   = 'Offer/index';

$route['beverage/delete/(:any)']     = 'Beverage/deletedata/$i';
$route['beverage/edit']              = 'Beverage/update';
$route['beverage/editview/(:any)']   = 'Beverage/update_view/$i';
$route['beverage/add']               = 'Beverage/insertdata';
$route['beverage_ad']                   = 'Beverage/index';

$route['burger/delete/(:any)']     = 'Burger/deletedata/$i';
$route['burger/edit']              = 'Burger/update';
$route['burger/editview/(:any)']   = 'Burger/update_view/$i';
$route['burger/add']               = 'Burger/insertdata';
$route['burger_ad']                   = 'Burger/index';

$route['dessert/delete/(:any)']     = 'Dessert/deletedata/$i';
$route['dessert/edit']              = 'Dessert/update';
$route['dessert/editview/(:any)']   = 'Dessert/update_view/$i';
$route['dessert/add']               = 'Dessert/insertdata';
$route['dessert_ad']                   = 'Dessert/index';

$route['mocktail/delete/(:any)']     = 'Mocktail/deletedata/$i';
$route['mocktail/edit']              = 'Mocktail/update';
$route['mocktail/editview/(:any)']   = 'Mocktail/update_view/$i';
$route['mocktail/add']               = 'Mocktail/insertdata';
$route['mocktail_ad']                   = 'Mocktail/index';

$route['pizza/delete/(:any)']        = 'Pizza/deletedata/$i';
$route['pizza/edit']              = 'Pizza/update';
$route['pizza/editview/(:any)']   = 'Pizza/update_view/$i';
$route['pizza/add']               = 'Pizza/insertdata';
$route['pizza_ad']                   = 'Pizza/index';

$route['sandwich/delete/(:any)']     = 'Sandwich/deletedata/$i';
$route['sandwich/edit']              = 'Sandwich/update';
$route['sandwich/editview/(:any)']   = 'Sandwich/update_view/$i';
$route['sandwich/add']               = 'Sandwich/insertdata';
$route['sandwich_ad']                   = 'Sandwich/index';
