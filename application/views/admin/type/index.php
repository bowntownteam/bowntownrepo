<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Product Sub Category
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Product Sub Category</h3>
                        <!-- for seession message -->
                        <?php if($this->session->flashdata('flash')) { ?>
                            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                                <?= $this->session->flashdata('flash')['message']; ?>
                            </div>
                        <?php } ?>
                        <span class="pull-right"><a href="<?=base_url(); ?>index.php/type/add/" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>Types</th>
                                <td>Category</td>
                                <td>Option</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = $this->uri->segment(3);
                            if(count($type)>0) {
                                foreach ($type as $r) {
                                    $tab_id = $r['id'];

                                    ?>
                                    <tr>
                                        <td><?= ++$i; ?></td>
                                        <td><?= $r['type']; ?></td>
                                        <td>
                                            <?= $r['category']; ?> </a>
                                        </td>
                                        <td>
                                            <a href="<?= site_url(); ?>/type/delete/<?= $tab_id; ?>" class="btn btn-danger btn-flat" onclick="return delete_type()"">Delete</a>
                                            <a href="<?= site_url(); ?>/type/edit_view/<?= $tab_id; ?>" class="btn btn-success btn-flat">Edit</a>
                                        </td>
                                    </tr>
                            <?php }
                            }else{?>
                                <tr><td colspan="3" align="center"> Sub category list is empty</td></tr>
                        <?php }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <?php $rowCount = count($type); ?>
                    <!--for pagination --->
                    <div class="row" align="center">
                        <?php if($rowCount!=0) { echo $this->pagination->create_links();} else {}; ?>
                    </div>
                    <!-- pagination end -->
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>