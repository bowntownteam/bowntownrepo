<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Product type
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Add New Product Type </h3>
                    </div>
                    <div class="box-body">
                        <?php foreach ($records as $key){?>
                    	<form action="<?php echo site_url(); ?>/type/editdata" enctype="multipart/form-data" method="post">
                         <div class="row">
                             <input type="hidden" name="editId" value="<?= $key->id?>">
                             <div class="col-lg-4 col-md-5 col-sm-5">
                                 <div class="form-group">
                                     <label for="category">Category<span style="color: #CC0000" >*</span></label>
                                    <select class="form-control" name="category" required>
                                        <option value="">select</option>
                                        <?php foreach ($category as $cat){?>
                                        <option value="<?php echo $cat['id']?>" <?php if($key->category_id==$cat['id']){echo 'selected';}?>><?php echo $cat['category']?></option>
                                        <?php }?>
                                    </select>
                                 </div>
                                <div class="form-group">
                                    <label for="name">Type<span style="color: #CC0000" >*</span></label>
                                    <input type="text" name="type" id="type" class="form-control" value="<?= $key->type;?>" required />
                                </div>
                             </div>
                         </div>
                         <div class="form-group">
                             <button type="submit" class="btn btn-primary btn-flat" name="submit">Save</button>
                         </div>
                        </form>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->
