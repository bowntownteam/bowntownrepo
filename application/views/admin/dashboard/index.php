<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12"><h4>Welcome to Brown Town admin panel</h4></div>
        </div>
        <!-- for seession message -->
        <?php if($this->session->flashdata('flash')) { ?>
            <div class="status status-<?= $this->session->flashdata('flash')['type']; ?>" data-role="auto-hide">
                <?= $this->session->flashdata('flash')['message']; ?>
            </div>
        <?php } ?>
        <p></p>
        <div class="row">
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                    	<h3><?php echo $category ?></h3>
                        <p>Product Categories</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                    <a href="<?php echo site_url() ?>/category" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-xs-6">
                <!-- small box -->
                <div class="small-box">
                    <div class="inner">
                        <h3><?php echo $product ?></h3>
                        <p>Product</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
<!--                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
                </div>
            </div>
            <div class="col-lg-12 col-xs-12" >
                <!-- small box -->
                <div class="small-box feedback_chart" style="height: auto !important;    background: #fff;">
                    <h3 class="text-center" style="margin-bottom: 50px;margin-top: 10px; font-size: 28px; font-weight: 500;    color: #000;">Feedback</h3>
                    <div style=" width: 100%; text-align: right;padding-right: 34px;">
                        <a href="<?= site_url(); ?>/home/feedclear" class="btn btn-danger btn-flat" onclick="return delete_type()">Clear all</a>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                            <div id="example"  class="pie-title-center" data-percent="<?php echo $feedback['love'] ?>">
                              <span class="pie-value" style="color: #000 !important;"></span>
                            </div>
                            <span><img src="<?= base_url(); ?>img/lov.png"></span>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                            <div id="example_1"  class="pie-title-center" data-percent="<?php echo $feedback['happy'] ?>">
                              <span class="pie-value" style="color: #000 !important;"></span>
                            </div>
                            <span><img src="<?= base_url(); ?>img/gd.png"></span>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 text-center">
                            <div id="example_2"  class="pie-title-center" data-percent="<?php echo $feedback['angry'] ?>">
                              <span class="pie-value" style="color: #000 !important;"></span>
                            </div>
                            <span><img src="<?= base_url(); ?>img/bad.png"></span>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        <style>
            .pie-title-center {
              display: inline-block;
              position: relative;
              text-align: center;/*
              background: #fff;*/
            }

            .pie-value {
              display: block;
              position: absolute;
              font-size: 14px;
              height: 40px;
              top: 50%;
              left: 0;
              right: 0;
              margin-top: -20px;
              line-height: 40px;
                color: #fff !important;
            }
            h4{
                color: #000;
            }
            .feedback_chart span{
                display: block;
            }
            .feedback_chart span img{
                max-width: 100%;
                width: 50px;
                padding-top: 20px;
            }
        </style>
<!--        <div class="row">-->
<!--            <div class="circleGraphic1 col-md-3 col-sm-6">75</div>-->
<!--            <div class="circleGraphic2 col-md-3 col-sm-6">20</div>-->
<!--            <div class="circleGraphic3 col-md-3 col-sm-6">20</div>-->
<!--            <div class="circleGraphic4 col-md-3 col-sm-6">67</div>-->
<!--        </div>-->

     </section>
</div>
<script>
    function delete_type()
    {
        var del=confirm("Do you Want to Delete ?");
        if(del==true)
        {
            window.submit();
        }
        else
        {
            return false;
        }
    }
</script>